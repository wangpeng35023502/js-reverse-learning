# -*- coding:utf-8 -*-
import frida, sys

hook_data = """
Java.perform(function () {
    send("start hook!")
    var l = Java.use('f.v.a.j.l')
    l.d.implementation = function (str1,jSONString) {
        console.log("str1---->:",str1)
        console.log("jSONString---->:",jSONString)
        console.log("data---->:",this.d(str1,jSONString))
        return this.d(str1,str2);
    };
    
});
"""


def on_message(message, data):
    if message['type'] == 'send':
        print("[*] {0}".format(message['payload']))
    else:
        print(message)


process = frida.get_usb_device().attach('com.showstartfans.activity')
script = process.create_script(hook_data)
script.on('message', on_message)
script.load()
sys.stdin.read()
