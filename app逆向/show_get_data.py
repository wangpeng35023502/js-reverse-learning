import hashlib
import base64
import time

from Crypto.Cipher import AES
from Crypto.Util.Padding import pad

import random
import requests
import json


def ranstr():
    """
    生成随机8位字符串
    :return:
    """
    seed = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    sa = []
    for i in range(8):
        sa.append(random.choice(seed))
    salt = ''.join(sa)
    return salt


def md5(sign):
    """
    md5加密
    :param sign:
    :return:
    """
    hash_md5 = hashlib.md5(sign.encode())
    sign_encrypt = hash_md5.hexdigest()
    return sign_encrypt


def get_data(data, sign):
    headers = {
        "Host": "pro2-api.showstart.com",
        "user-agent": "Mozilla/5.0 (Linux; Android 8.1.0; Nexus 6P Build/OPM7.181205.001; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/61.0.3163.98 Mobile Safari/537.36",
        "cterminal": "android",
        "cusystime": "1673678995193",
        "cuuserref": "16f86ff00de61a9e6acd21ea7a95a5f2",
        "cusut": "",
        "content-type": "application/json;charset=UTF-8"
    }
    url = "https://pro2-api.showstart.com/app/000000000000"
    data = {
        "appid": "app",
        "aru": "GVKX4Q+QwV6pIGyo2mowT1cfHWWprgS6gS91f/6yHvk=",
        "data": data,
        "sign": sign,
        "terminal": "android",
        "version": "5.0.1"
    }
    data = json.dumps(data)
    response = requests.post(url, headers=headers, data=data)

    print(response.json())


class EncryptDate:
    def __init__(self, key):
        # 初始化密钥
        self.key = key
        # 初始化数据块大小
        self.length = AES.block_size
        # 初始化AES,ECB模式的实例
        self.aes = AES.new(self.key.encode("utf-8"), AES.MODE_ECB)
        # 截断函数，去除填充的字符
        self.unpad = lambda date: date[0:-ord(date[-1])]

    def fill_method(self, aes_str):
        '''pkcs7补全'''
        pad_pkcs7 = pad(aes_str.encode('utf-8'), AES.block_size, style='pkcs7')

        return pad_pkcs7

    def encrypt(self, encrData):
        # 加密函数,使用pkcs7补全
        res = self.aes.encrypt(self.fill_method(encrData))
        # 转换为base64
        msg = str(base64.b64encode(res), encoding="utf-8")

        return msg

    def decrypt(self, decrData):
        # base64解码
        res = base64.decodebytes(decrData.encode("utf-8"))
        # 解密函数
        msg = self.aes.decrypt(res).decode("utf-8")

        return self.unpad(msg)


if __name__ == '__main__':
    qtime = str(time.time() * 1000)

    info = '' #JSONString
    key = ''  # key
    eg = EncryptDate(key)
    sign = md5(info)
    data = eg.encrypt(info)
    get_data(data, sign)
