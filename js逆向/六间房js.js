/*
https://v.6.cn/
*/
var L= 8
var M = function(S) {
            var T = Array();
            var V = (1 << L) - 1;
            for (var U = 0; U < S.length * L; U += L) {
                T[U >> 5] |= (S.charCodeAt(U / L) & V) << (24 - U % 32)
            }
            return T
        };
var N = function(V, S) {
            var T = (V & 65535) + (S & 65535);
            var U = (V >> 16) + (S >> 16) + (T >> 16);
            return (U << 16) | (T & 65535)
        };
var O = function(T, S) {
            return (T << S) | (T >>> (32 - S))
        };
var K = function (T) {
    var U = "0123456789abcdef";
    var S = "";
    for (var V = 0; V < T.length * 4; V++) {
        S += U.charAt((T[V >> 2] >> ((3 - V % 4) * 8 + 4)) & 15) + U.charAt((T[V >> 2] >> ((3 - V % 4) * 8)) & 15)
    }
    return S
};
var Q = function(Y, S) {
            Y[S >> 5] |= 128 << (24 - S % 32);
            Y[((S + 64 >> 9) << 4) + 15] = S;
            var W = Array(80);
            var ad = 1732584193;
            var ae = -271733879;
            var af = -1732584194;
            var ag = 271733878;
            var ah = -1009589776;
            for (var X = 0; X < Y.length; X += 16) {
                var U = ad;
                var V = ae;
                var Z = af;
                var aa = ag;
                var ac = ah;
                for (var ab = 0; ab < 80; ab++) {
                    if (ab < 16) {
                        W[ab] = Y[X + ab]
                    } else {
                        W[ab] = O(W[ab - 3] ^ W[ab - 8] ^ W[ab - 14] ^ W[ab - 16], 1)
                    }
                    var T = N(N(O(ad, 5), R(ab, ae, af, ag)), N(N(ah, W[ab]), P(ab)));
                    ah = ag;
                    ag = af;
                    af = O(ae, 30);
                    ae = ad;
                    ad = T
                }
                ad = N(ad, U);
                ae = N(ae, V);
                af = N(af, Z);
                ag = N(ag, aa);
                ah = N(ah, ac)
            }
            return Array(ad, ae, af, ag, ah)
        };
var R = function(U, V, S, T) {
            if (U < 20) {
                return (V & S) | ((~V) & T)
            }
            if (U < 40) {
                return V ^ S ^ T
            }
            if (U < 60) {
                return (V & S) | (V & T) | (S & T)
            }
            return V ^ S ^ T
        };
var P = function(S) {
            return (S < 20) ? 1518500249 : (S < 40) ? 1859775393 : (S < 60) ? -1894007588 : -899497514
        };
hex_sha1 = function (S) {
    return K(Q(M(S), S.length * L))
}
S = {
    encode: function (J) {
        return hex_sha1(J)
    },
    servertime: parseInt(Date.parse(new Date()).toString().substr(0,10))
}

function getPwd(pwd) {
    return S.encode("" + S.encode(S.encode(pwd) + S.servertime) + S.nonce)
};

console.log(getPwd('111111'))
