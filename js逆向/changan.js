/*
* https://bqcm0.cavip1.com/
* */
const CryptoJS = require('crypto-js')
var utils = {
    dynamicSort: function(property) {
        var sortOrder = 1;
        return "-" === property[0] && (sortOrder = -1,
        property = property.substr(1)),
        function(a, b) {
            var result = 0;
            return result = property.split(".").length ? eval("a." + property) < eval("b." + property) ? -1 : eval("a." + property) > eval("b." + property) ? 1 : 0 : a[property] < b[property] ? -1 : a[property] > b[property] ? 1 : 0,
            result * sortOrder
        }
    },
    desEncrypt: function(e, t) {
        var n = CryptoJS.enc.Utf8.parse(t);
        return CryptoJS.DES.encrypt(e, n, {
            mode: CryptoJS.mode.ECB,
            padding: CryptoJS.pad.Pkcs7
        }).toString()
    },
    rndString: function() {
        for (var e = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz", t = "", n = 0; n < 16; n++) {
            var a = Math.floor(Math.random() * e.length);
            t += e.substring(a, a + 1)
        }
        return t
    },
    getDesKey: function() {
        return this._desKey = this._desKey ? this._desKey : this.rndString()
    },
    getRSAEncrypt: function() {
        return this._encryption = this._encryption ? this._encryption : RSAEncrypt(this.getDesKey().split("").reverse().join(""))
    },
    getDay: function(e) {
        var t = new Date(Date.now() + 1e3 * e * 60 * 60 * 24);
        return [new Date(t.getFullYear(),t.getMonth(),t.getDate(),0,0,0), new Date(t.getFullYear(),t.getMonth(),t.getDate(),23,59,59)]
    },
    getDateDifferences: function(e, t) {
        return Math.floor((Date.UTC(e.getFullYear(), e.getMonth(), e.getDate()) - Date.UTC(t.getFullYear(), t.getMonth(), t.getDate())) / 864e5)
    },
    transferFullToHalf: function(e) {
        var t = "";
        if (NaN == e || null == e || "" == e || null == e)
            return t;
        for (var n = 0, a = e.length; n < a; n++)
            12288 == e.charCodeAt(n) ? t += " " : 9 == e.charCodeAt(n) ? t += " " : e.charCodeAt(n) > 65280 && e.charCodeAt(n) < 65375 ? t += String.fromCharCode(e.charCodeAt(n) - 65248) : t += String.fromCharCode(e.charCodeAt(n));
        return t
    },
    checkDiffNow: function(e, t, n) {
        var a = this.getDateDifferences(e, t)
          , i = Math.abs(a).format();
        if (i >= 30 && i < 365)
            return Math.floor(i / 30) + "个月前";
        if (i < 30 && i >= 1)
            return i + "天前";
        if (i > 365)
            return "1年前";
        if (null == n || null == n)
            return "未登录";
        if (0 == i) {
            var r = new Date
              , o = parseInt((r.getTime() - t.getTime()) / 1e3) % 31536e3 % 2592e3 % 86400
              , s = o % 3600
              , l = Math.floor(o / 3600)
              , d = Math.floor(s / 60);
            if (l > 0)
                return l + "小时前";
            if (0 == l && d > 0)
                return d + "分钟前";
            if (0 == l && 0 == d)
                return "刚刚"
        }
    },
    copyToClipBoard: function(e) {
        var t = $('<textarea id="clipBoardInput" style="height:1px;"/>');
        t.appendTo("body").val(e).select(),
        document.execCommand("copy"),
        t.remove()
    },
    parseUrlHash: function() {
        var e, t, n = {
            url: null,
            params: {}
        }, a = location.hash;
        if (String.isNotEmpty(a) && (a = a.replace("#/", "").split("?"),
        n.url = a[0],
        a.length >= 2)) {
            e = a[1].split("&");
            for (var i = 0; i < e.length; i++)
                t = e[i].split("="),
                n.params[t[0]] = t[1]
        }
        return n
    },
    maskString: function(e, t) {
        for (var n = e.substring(0, t), a = 0; a < e.length; a++)
            a >= t && (n += "*");
        return n
    },
    checkBankAccount: function(e) {
        if (e.length < 16 || e.length > 19)
            return {
                status: !1,
                message: "银行卡号长度必须在16到19之间"
            };
        if (!/^\d*$/.exec(e))
            return {
                status: !1,
                message: "银行卡号必须全为数字"
            };
        if (-1 == "10,18,30,35,37,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,58,60,62,65,68,69,84,87,88,94,95,98,99".indexOf(e.substring(0, 2)))
            return {
                status: !1,
                message: "银行卡开头6位不符号规范"
            };
        for (var t = e.substr(e.length - 1, 1), n = e.substr(0, e.length - 1), a = new Array, i = n.length - 1; i > -1; i--)
            a.push(n.substr(i, 1));
        for (var r = new Array, o = new Array, s = new Array, l = 0; l < a.length; l++)
            (l + 1) % 2 == 1 ? 2 * parseInt(a[l]) < 9 ? r.push(2 * parseInt(a[l])) : o.push(2 * parseInt(a[l])) : s.push(a[l]);
        for (var d = new Array, u = new Array, c = 0; c < o.length; c++)
            d.push(parseInt(o[c]) % 10),
            u.push(parseInt(o[c]) / 10);
        for (var m, p = 0, f = 0, h = 0, g = 0, _ = 0; _ < r.length; _++)
            p += parseInt(r[_]);
        for (var b = 0; b < s.length; b++)
            f += parseInt(s[b]);
        for (var v = 0; v < d.length; v++)
            h += parseInt(d[v]),
            g += parseInt(u[v]);
        return m = parseInt(p) + parseInt(f) + parseInt(h) + parseInt(g),
        t == 10 - (parseInt(m) % 10 == 0 ? 10 : parseInt(m) % 10) ? {
            status: !0
        } : {
            status: !1,
            message: "验证失败"
        }
    },
    getRebatePercentage: function(e, t) {
        var n = e || {}
          , a = t || 0
          , i = 0;
        switch (n.game_type) {
        case "SSC_1":
            i = Math.round((n.max - a) / 2e3 * 100 * 10) / 10
        }
        return " / " + i + "%"
    },
    getUrlParam: function(e, t) {
        var n, a = t;
        return ~window.location.href.indexOf(e) && (a = (n = {},
        window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(e, t, a) {
            n[t] = a
        }),
        n)[e]),
        a
    },
    filterSSCGame: function(e) {
        var t, n, a, i = [], r = [];
        if (e.length <= 0)
            return e;
        for (var o = 0, s = e.length; o < s; o++) {
            if ("SSC" === e[o].code) {
                a = o;
                for (var l = 0, d = (t = e[o].games).length; l < d; l++)
                    -1 != t[l].categories.indexOf("STEAM_GAME") ? i.push(t[l]) : r.push(t[l])
            }
            e[o].games = r;
            break
        }
        return !i.length <= 0 && (n = {
            gameGroupId: e.length + 2,
            code: "SSCGAME",
            displayName: "游戏彩",
            games: i,
            remark: "游戏彩"
        }),
        n ? (e.splice(a + 1, 0, n),
        e) : e
    },
    getGameData: function(e, t) {
        var n, a, i, r, o = {};
        n = {
            username: user.getUsername(),
            merchantCode: global.merchant,
            token: user.getToken()
        },
        $.extend(o, e, n),
        a = $.param(o),
        r = (i = global.launch_url)[Math.floor(Math.random() * i.length)] + "/player-wps.html?" + a,
        console.log(r),
        t.location.replace(r),
        t.focus()
    },
    filterEmoji: function(e) {
        for (var t, n = [], a = [], i = "", r = e.replace(regexFilter.emoji, "&"), o = 0, s = r.length; o < s; o++)
            "&" === r[o] && n.push(o);
        for (var l = 0, d = n.length; l < d; l++)
            0 === l ? t = e.substring(n[l], n[l] + 2) : (n[l] = n[l] + l,
            t = e.substring(n[l], n[l] + 2)),
            emojiObjReceive[this.encodeUnicode(t)] ? (emoji = emojiObjReceive[this.encodeUnicode(t)] ? emojiObjReceive[this.encodeUnicode(t)].url : t,
            a.push(emoji)) : a.push(t);
        var u = -1;
        for (o = 0,
        s = r.length; o < s; o++)
            i += "&" === r[o] ? a[++u].split(".").length >= 2 ? '<img src="../../images/chat/emoji/' + a[u] + '">' : a[u] : r[o];
        return i
    },
    getAge: function(e) {
        if (!e)
            return 0;
        monthDay = new Date(e).format("yyyy-MM-dd").split("-");
        var t = (e = (new Date).format("yyyy-MM-dd").split("-"))[0] - monthDay[0];
        return t >= 1 && (e[1] - monthDay[1] == 0 ? e[2] - monthDay[2] < 0 && (t -= 1) : e[1] - monthDay[1] <= 0 && (t -= 1)),
        {
            year: monthDay[0],
            month: monthDay[1],
            day: monthDay[2],
            age: t
        }
    },
    encodeUnicode: function(e) {
        for (var t = [], n = 0; n < e.length; n++)
            t[n] = ("00" + e.charCodeAt(n).toString(16)).slice(-4);
        return "\\u" + t.join("\\u")
    },
    urlTurnQRCode: function(e) {
        (e = {
            element: e.element || "",
            url: e.url || "",
            width: e.width || 244,
            height: e.height || 244
        }).element ? e.url ? ($("" + e.element).html(" "),
        $("" + e.element).qrcode({
            render: "canvas",
            width: e.width,
            height: e.height,
            text: function(e) {
                var t, n, a, i;
                for (t = "",
                a = e.length,
                n = 0; n < a; n++)
                    (i = e.charCodeAt(n)) >= 1 && i <= 127 ? t += e.charAt(n) : i > 2047 ? (t += String.fromCharCode(224 | i >> 12 & 15),
                    t += String.fromCharCode(128 | i >> 6 & 63),
                    t += String.fromCharCode(128 | i >> 0 & 63)) : (t += String.fromCharCode(192 | i >> 6 & 31),
                    t += String.fromCharCode(128 | i >> 0 & 63));
                return t
            }(e.url),
            typeNumber: -1,
            correctLevel: 2,
            background: "#ffffff",
            foreground: "#000000"
        })) : console.error("请传入需要转对url") : console.error("请传入需要插入对DOM")
    },
    makerandomid: function(e) {
        for (var t = "", n = "0123456789".length, a = 0; a < e; a++)
            t += "0123456789".charAt(Math.floor(Math.random() * n));
        return t
    },
    getPercent: function(e, t) {
        return e = parseFloat(e),
        t = parseFloat(t),
        isNaN(e) || isNaN(t) ? "-" : parseFloat(t <= 0 ? 0 : Math.round(e / t * 1e4) / 100).toFixed()
    },
    formatLast2: function(e, t) {
        return 1 * parseFloat(e).toFixed(4).slice(0, t || -2)
    },
    getDateDiff: function(e) {
        var t = e - Date.now()
          , n = parseInt(t / 36e5);
        n = n < 10 ? "0" + n : n;
        var a = parseInt(t / 6e4 % 60);
        a = a < 10 ? "0" + a : a;
        var i = parseInt(t / 1e3 % 60);
        return n + "小时" + a + "分" + (i = i < 10 ? "0" + i : i) + "秒"
    },
    timeDifference: function(e, t) {
        var n = {
            days: "",
            hours: "",
            minutes: ""
        }
          , a = Date.parse(new Date(e))
          , i = Date.parse(new Date(t)) - a
          , r = Math.floor(i / 864e5);
        n.days = r;
        var o = i % 864e5
          , s = Math.floor(o / 36e5);
        n.hours = s;
        var l = o % 36e5
          , d = Math.floor(l / 6e4);
        n.minutes = d;
        return n
    }
};
function RSAKeyPair(e, t, n) {
    this.e = biFromHex(e),
    this.d = biFromHex(t),
    this.m = biFromHex(n),
    this.chunkSize = 2 * biHighIndex(this.m),
    this.radix = 16,
    this.barrett = new BarrettMu(this.m)
}
function twoDigit(e) {
    return (e < 10 ? "0" : "") + String(e)
}
function encryptedString(e, t) {
    for (var n = new Array, a = t.length, i = 0; i < a; )
        n[i] = t.charCodeAt(i),
        i++;
    for (; n.length % e.chunkSize != 0; )
        n[i++] = 0;
    var r, o, s, l = n.length, d = "";
    for (i = 0; i < l; i += e.chunkSize) {
        for (s = new BigInt,
        r = 0,
        o = i; o < i + e.chunkSize; ++r)
            s.digits[r] = n[o++],
            s.digits[r] += n[o++] << 8;
        var u = e.barrett.powMod(s, e.e);
        d += (16 == e.radix ? biToHex(u) : biToString(u, e.radix)) + " "
    }
    return d.substring(0, d.length - 1)
}
function desDecrypt(e, t) {
    var n = CryptoJS.enc.Utf8.parse(t);
    return CryptoJS.DES.decrypt({
        ciphertext: CryptoJS.enc.Base64.parse(e)
    }, n, {
        mode: CryptoJS.mode.ECB,
        padding: CryptoJS.pad.Pkcs7
    }).toString(CryptoJS.enc.Utf8)
}
var biRadixBase = 2, biRadixBits = 16, bitsPerDigit = biRadixBits, biRadix = 65536, biHalfRadix = biRadix >>> 1, biRadixSquared = biRadix * biRadix, maxDigitVal = biRadix - 1, maxInteger = 9999999999999998, maxDigits, ZERO_ARRAY, bigZero, bigOne;
function setMaxDigits(e) {
    ZERO_ARRAY = new Array(maxDigits = e);
    for (var t = 0; t < ZERO_ARRAY.length; t++)
        ZERO_ARRAY[t] = 0;
    bigZero = new BigInt,
    (bigOne = new BigInt).digits[0] = 1
}
setMaxDigits(20);
var dpl10 = 15
  , lr10 = biFromNumber(1e15);
function BigInt(e) {
    this.digits = "boolean" == typeof e && 1 == e ? null : ZERO_ARRAY.slice(0),
    this.isNeg = !1
}
function biFromDecimal(e) {
    for (var t, n = "-" == e.charAt(0), a = n ? 1 : 0; a < e.length && "0" == e.charAt(a); )
        ++a;
    if (a == e.length)
        t = new BigInt;
    else {
        var i = (e.length - a) % dpl10;
        for (0 == i && (i = dpl10),
        t = biFromNumber(Number(e.substr(a, i))),
        a += i; a < e.length; )
            t = biAdd(biMultiply(t, lr10), biFromNumber(Number(e.substr(a, dpl10)))),
            a += dpl10;
        t.isNeg = n
    }
    return t
}
function biCopy(e) {
    var t = new BigInt(!0);
    return t.digits = e.digits.slice(0),
    t.isNeg = e.isNeg,
    t
}
function biFromNumber(e) {
    var t = new BigInt;
    t.isNeg = e < 0,
    e = Math.abs(e);
    for (var n = 0; e > 0; )
        t.digits[n++] = e & maxDigitVal,
        e = Math.floor(e / biRadix);
    return t
}
function reverseStr(e) {
    for (var t = "", n = e.length - 1; n > -1; --n)
        t += e.charAt(n);
    return t
}
var hexatrigesimalToChar = new Array("0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z");
function biToString(e, t) {
    var n = new BigInt;
    n.digits[0] = t;
    for (var a = biDivideModulo(e, n), i = hexatrigesimalToChar[a[1].digits[0]]; 1 == biCompare(a[0], bigZero); )
        a = biDivideModulo(a[0], n),
        digit = a[1].digits[0],
        i += hexatrigesimalToChar[a[1].digits[0]];
    return (e.isNeg ? "-" : "") + reverseStr(i)
}
function biToDecimal(e) {
    var t = new BigInt;
    t.digits[0] = 10;
    for (var n = biDivideModulo(e, t), a = String(n[1].digits[0]); 1 == biCompare(n[0], bigZero); )
        n = biDivideModulo(n[0], t),
        a += String(n[1].digits[0]);
    return (e.isNeg ? "-" : "") + reverseStr(a)
}
var hexToChar = new Array("0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f");
function digitToHex(e) {
    var t = "";
    for (i = 0; i < 4; ++i)
        t += hexToChar[15 & e],
        e >>>= 4;
    return reverseStr(t)
}
function biToHex(e) {
    for (var t = "", n = (biHighIndex(e),
    biHighIndex(e)); n > -1; --n)
        t += digitToHex(e.digits[n]);
    return t
}
function charToHex(e) {
    return e >= 48 && e <= 57 ? e - 48 : e >= 65 && e <= 90 ? 10 + e - 65 : e >= 97 && e <= 122 ? 10 + e - 97 : 0
}
function hexToDigit(e) {
    for (var t = 0, n = Math.min(e.length, 4), a = 0; a < n; ++a)
        t <<= 4,
        t |= charToHex(e.charCodeAt(a));
    return t
}
function biFromHex(e) {
    for (var t = new BigInt, n = e.length, a = 0; n > 0; n -= 4,
    ++a)
        t.digits[a] = hexToDigit(e.substr(Math.max(n - 4, 0), Math.min(n, 4)));
    return t
}
function biFromString(e, t) {
    var n = "-" == e.charAt(0)
      , a = n ? 1 : 0
      , i = new BigInt
      , r = new BigInt;
    r.digits[0] = 1;
    for (var o = e.length - 1; o >= a; o--) {
        i = biAdd(i, biMultiplyDigit(r, charToHex(e.charCodeAt(o)))),
        r = biMultiplyDigit(r, t)
    }
    return i.isNeg = n,
    i
}
function biDump(e) {
    return (e.isNeg ? "-" : "") + e.digits.join(" ")
}
function biAdd(e, t) {
    var n;
    if (e.isNeg != t.isNeg)
        t.isNeg = !t.isNeg,
        n = biSubtract(e, t),
        t.isNeg = !t.isNeg;
    else {
        n = new BigInt;
        for (var a, i = 0, r = 0; r < e.digits.length; ++r)
            a = e.digits[r] + t.digits[r] + i,
            n.digits[r] = a % biRadix,
            i = Number(a >= biRadix);
        n.isNeg = e.isNeg
    }
    return n
}
function biSubtract(e, t) {
    var n;
    if (e.isNeg != t.isNeg)
        t.isNeg = !t.isNeg,
        n = biAdd(e, t),
        t.isNeg = !t.isNeg;
    else {
        var a, i;
        n = new BigInt,
        i = 0;
        for (var r = 0; r < e.digits.length; ++r)
            a = e.digits[r] - t.digits[r] + i,
            n.digits[r] = a % biRadix,
            n.digits[r] < 0 && (n.digits[r] += biRadix),
            i = 0 - Number(a < 0);
        if (-1 == i) {
            i = 0;
            for (r = 0; r < e.digits.length; ++r)
                a = 0 - n.digits[r] + i,
                n.digits[r] = a % biRadix,
                n.digits[r] < 0 && (n.digits[r] += biRadix),
                i = 0 - Number(a < 0);
            n.isNeg = !e.isNeg
        } else
            n.isNeg = e.isNeg
    }
    return n
}
function biHighIndex(e) {
    for (var t = e.digits.length - 1; t > 0 && 0 == e.digits[t]; )
        --t;
    return t
}
function biNumBits(e) {
    var t, n = biHighIndex(e), a = e.digits[n], i = (n + 1) * bitsPerDigit;
    for (t = i; t > i - bitsPerDigit && 0 == (32768 & a); --t)
        a <<= 1;
    return t
}
function biMultiply(e, t) {
    for (var n, a, i, r = new BigInt, o = biHighIndex(e), s = biHighIndex(t), l = 0; l <= s; ++l) {
        for (n = 0,
        i = l,
        j = 0; j <= o; ++j,
        ++i)
            a = r.digits[i] + e.digits[j] * t.digits[l] + n,
            r.digits[i] = a & maxDigitVal,
            n = a >>> biRadixBits;
        r.digits[l + o + 1] = n
    }
    return r.isNeg = e.isNeg != t.isNeg,
    r
}
function biMultiplyDigit(e, t) {
    var n, a, i;
    result = new BigInt,
    n = biHighIndex(e),
    a = 0;
    for (var r = 0; r <= n; ++r)
        i = result.digits[r] + e.digits[r] * t + a,
        result.digits[r] = i & maxDigitVal,
        a = i >>> biRadixBits;
    return result.digits[1 + n] = a,
    result
}
function arrayCopy(e, t, n, a, i) {
    for (var r = Math.min(t + i, e.length), o = t, s = a; o < r; ++o,
    ++s)
        n[s] = e[o]
}
var highBitMasks = new Array(0,32768,49152,57344,61440,63488,64512,65024,65280,65408,65472,65504,65520,65528,65532,65534,65535);
function biShiftLeft(e, t) {
    var n = Math.floor(t / bitsPerDigit)
      , a = new BigInt;
    arrayCopy(e.digits, 0, a.digits, n, a.digits.length - n);
    for (var i = t % bitsPerDigit, r = bitsPerDigit - i, o = a.digits.length - 1, s = o - 1; o > 0; --o,
    --s)
        a.digits[o] = a.digits[o] << i & maxDigitVal | (a.digits[s] & highBitMasks[i]) >>> r;
    return a.digits[0] = a.digits[o] << i & maxDigitVal,
    a.isNeg = e.isNeg,
    a
}
var lowBitMasks = new Array(0,1,3,7,15,31,63,127,255,511,1023,2047,4095,8191,16383,32767,65535);
function biShiftRight(e, t) {
    var n = Math.floor(t / bitsPerDigit)
      , a = new BigInt;
    arrayCopy(e.digits, n, a.digits, 0, e.digits.length - n);
    for (var i = t % bitsPerDigit, r = bitsPerDigit - i, o = 0, s = o + 1; o < a.digits.length - 1; ++o,
    ++s)
        a.digits[o] = a.digits[o] >>> i | (a.digits[s] & lowBitMasks[i]) << r;
    return a.digits[a.digits.length - 1] >>>= i,
    a.isNeg = e.isNeg,
    a
}
function biMultiplyByRadixPower(e, t) {
    var n = new BigInt;
    return arrayCopy(e.digits, 0, n.digits, t, n.digits.length - t),
    n
}
function biDivideByRadixPower(e, t) {
    var n = new BigInt;
    return arrayCopy(e.digits, t, n.digits, 0, n.digits.length - t),
    n
}
function biModuloByRadixPower(e, t) {
    var n = new BigInt;
    return arrayCopy(e.digits, 0, n.digits, 0, t),
    n
}
function biCompare(e, t) {
    if (e.isNeg != t.isNeg)
        return 1 - 2 * Number(e.isNeg);
    for (var n = e.digits.length - 1; n >= 0; --n)
        if (e.digits[n] != t.digits[n])
            return e.isNeg ? 1 - 2 * Number(e.digits[n] > t.digits[n]) : 1 - 2 * Number(e.digits[n] < t.digits[n]);
    return 0
}
function biDivideModulo(e, t) {
    var n, a, i = biNumBits(e), r = biNumBits(t), o = t.isNeg;
    if (i < r)
        return e.isNeg ? ((n = biCopy(bigOne)).isNeg = !t.isNeg,
        e.isNeg = !1,
        t.isNeg = !1,
        a = biSubtract(t, e),
        e.isNeg = !0,
        t.isNeg = o) : (n = new BigInt,
        a = biCopy(e)),
        new Array(n,a);
    n = new BigInt,
    a = e;
    for (var s = Math.ceil(r / bitsPerDigit) - 1, l = 0; t.digits[s] < biHalfRadix; )
        t = biShiftLeft(t, 1),
        ++l,
        ++r,
        s = Math.ceil(r / bitsPerDigit) - 1;
    a = biShiftLeft(a, l),
    i += l;
    for (var d = Math.ceil(i / bitsPerDigit) - 1, u = biMultiplyByRadixPower(t, d - s); -1 != biCompare(a, u); )
        ++n.digits[d - s],
        a = biSubtract(a, u);
    for (var c = d; c > s; --c) {
        var m = c >= a.digits.length ? 0 : a.digits[c]
          , p = c - 1 >= a.digits.length ? 0 : a.digits[c - 1]
          , f = c - 2 >= a.digits.length ? 0 : a.digits[c - 2]
          , h = s >= t.digits.length ? 0 : t.digits[s]
          , g = s - 1 >= t.digits.length ? 0 : t.digits[s - 1];
        n.digits[c - s - 1] = m == h ? maxDigitVal : Math.floor((m * biRadix + p) / h);
        for (var _ = n.digits[c - s - 1] * (h * biRadix + g), b = m * biRadixSquared + (p * biRadix + f); _ > b; )
            --n.digits[c - s - 1],
            _ = n.digits[c - s - 1] * (h * biRadix | g),
            b = m * biRadix * biRadix + (p * biRadix + f);
        (a = biSubtract(a, biMultiplyDigit(u = biMultiplyByRadixPower(t, c - s - 1), n.digits[c - s - 1]))).isNeg && (a = biAdd(a, u),
        --n.digits[c - s - 1])
    }
    return a = biShiftRight(a, l),
    n.isNeg = e.isNeg != o,
    e.isNeg && (n = o ? biAdd(n, bigOne) : biSubtract(n, bigOne),
    a = biSubtract(t = biShiftRight(t, l), a)),
    0 == a.digits[0] && 0 == biHighIndex(a) && (a.isNeg = !1),
    new Array(n,a)
}
function biDivide(e, t) {
    return biDivideModulo(e, t)[0]
}
function biModulo(e, t) {
    return biDivideModulo(e, t)[1]
}
function biMultiplyMod(e, t, n) {
    return biModulo(biMultiply(e, t), n)
}
function biPow(e, t) {
    for (var n = bigOne, a = e; 0 != (1 & t) && (n = biMultiply(n, a)),
    0 != (t >>= 1); )
        a = biMultiply(a, a);
    return n
}
function biPowMod(e, t, n) {
    for (var a = bigOne, i = e, r = t; 0 != (1 & r.digits[0]) && (a = biMultiplyMod(a, i, n)),
    0 != (r = biShiftRight(r, 1)).digits[0] || 0 != biHighIndex(r); )
        i = biMultiplyMod(i, i, n);
    return a
}
function BarrettMu(e) {
    this.modulus = biCopy(e),
    this.k = biHighIndex(this.modulus) + 1;
    var t = new BigInt;
    t.digits[2 * this.k] = 1,
    this.mu = biDivide(t, this.modulus),
    this.bkplus1 = new BigInt,
    this.bkplus1.digits[this.k + 1] = 1,
    this.modulo = BarrettMu_modulo,
    this.multiplyMod = BarrettMu_multiplyMod,
    this.powMod = BarrettMu_powMod
}
function BarrettMu_modulo(e) {
    var t = biDivideByRadixPower(e, this.k - 1)
      , n = biDivideByRadixPower(biMultiply(t, this.mu), this.k + 1)
      , a = biSubtract(biModuloByRadixPower(e, this.k + 1), biModuloByRadixPower(biMultiply(n, this.modulus), this.k + 1));
    a.isNeg && (a = biAdd(a, this.bkplus1));
    for (var i = biCompare(a, this.modulus) >= 0; i; )
        i = biCompare(a = biSubtract(a, this.modulus), this.modulus) >= 0;
    return a
}
function BarrettMu_multiplyMod(e, t) {
    var n = biMultiply(e, t);
    return this.modulo(n)
}
function BarrettMu_powMod(e, t) {
    var n = new BigInt;
    n.digits[0] = 1;
    for (var a = e, i = t; 0 != (1 & i.digits[0]) && (n = this.multiplyMod(n, a)),
    0 != (i = biShiftRight(i, 1)).digits[0] || 0 != biHighIndex(i); )
        a = this.multiplyMod(a, a);
    return n
}
CryptoJS.mode.ECB = function() {
    var e = CryptoJS.lib.BlockCipherMode.extend();
    return e.Encryptor = e.extend({
        processBlock: function(e, t) {
            this._cipher.encryptBlock(e, t)
        }
    }),
    e.Decryptor = e.extend({
        processBlock: function(e, t) {
            this._cipher.decryptBlock(e, t)
        }
    }),
    e
}()
function RSAEncrypt(e) {
    var t = '96034a78b110c8460252a93fdf4bbd1c621ba3f4450c8fc56dfe94a39243e290b05a86701b2793684a3765c4da028020fae98e88c4ad08b8d292a8f6ea23c156603593e3ebdf870322c6377def575baeb522a9152fe4b64ca70b79195ae5772003a939e3393281974ecf93981a115fea5e2aca412d199add17c84c9798624105';
    return null == t ? null : (setMaxDigits(130),
    encryptedString(new RSAKeyPair("10001","",t), e))
}



function encryption() {
var i = utils.rndString();
return RSAEncrypt(i.split("").reverse().join(""))
}

console.log(encryption());