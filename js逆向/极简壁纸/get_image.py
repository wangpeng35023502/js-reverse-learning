﻿import json

import requests
import execjs
import os

PATH = 'img\\'
os.path.exists(PATH) or os.mkdir(PATH)


def get_decrypt_data(data):
    with open('demo.js', 'r', encoding='utf-8') as f:
        js = f.read()
    return execjs.compile(js).call('decrypt_data', data)


def join_url(data):
    with open('demo.js', 'r', encoding='utf-8') as f:
        js = f.read()
    return execjs.compile(js).call('getUrlParams', data)


url = "https://api.zzzmh.cn/bz/v3/getData"

data = {
    "category": "0",
    "categoryId": "0",
    "color": "0",
    "current": "1",
    "resolution": "0",
    "size": "24",
    "sort": "0",
}
headers = {
    'authority': 'api.zzzmh.cn',
    'accept': 'application/json, text/plain, */*',
    'accept-language': 'zh,zh-CN;q=0.9',
    'cache-control': 'no-cache',
    'chuck': 'a9c6893f22fb45399097d3ce553dcd57',
    'content-type': 'application/json;charset=UTF-8',
    'origin': 'https://bz.zzzmh.cn',
    'pragma': 'no-cache',
    'referer': 'https://bz.zzzmh.cn/',
    'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="100", "Google Chrome";v="100"',
    'sec-ch-ua-mobile': '?0',
    'sec-ch-ua-platform': '"Windows"',
    'sec-fetch-dest': 'empty',
    'sec-fetch-mode': 'cors',
    'sec-fetch-site': 'same-site',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.60 Safari/537.36'
}

response = requests.request("POST", url, headers=headers, json=data).json()['result']
img_id_list = json.loads(get_decrypt_data(response))
for i in img_id_list['list']:
    img_url = join_url(i['i'])
    download_img = requests.get(url=img_url)
    with open(PATH+i['i']+'.jpg','wb') as f:
        f.write(download_img.content)
