import requests
import execjs
import json

with open('安徽水信息.js', mode='r', encoding='utf-8') as f:
    js_code = execjs.compile(f.read())
data1 = js_code.call('paramsData', "2021091308", "2021091421", "A:10,25,50,100")
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36',
    'Origin': 'http://yc.wswj.net',
    'Referer': 'http://yc.wswj.net/',
}
data = data1
response = requests.post('http://61.191.22.196:5566/AHSXX/service/PublicBusinessHandler.ashx',
                         headers=headers,
                         data=data).json()['data']

result = js_code.call('decodeWord', response)
print(json.loads(result))
