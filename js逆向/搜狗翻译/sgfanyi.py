import requests
import execjs


def get_key_info(word):
    with open('sgTr.js', 'r', encoding='utf-8') as f:
        js_code = f.read()
    result = execjs.compile(js_code).call('fanyi', word)
    return word, result


def fanyi(words):
    url = "https://fanyi.sogou.com/api/transpc/text/result"
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36',
        'Origin': 'https://fanyi.sogou.com',
    }
    cookies = {
        'Cookie':''
    }
    data = {
        'client': "pc",
        'exchange': 'false',
        'fr': "browser_pc",
        'from': "auto",
        'needQc': '1',
        's': words[1],
        'text': words[0],
        'to': "en",
        'uuid': "619a2a26-1a10-4e03-9636-7fe8c08359d1"
    }
    json_data = requests.post(url=url, headers=headers, data=data,cookies=cookies).json()
    return json_data


if __name__ == '__main__':
    key_word = '你好'
    words = get_key_info(key_word)
    print(fanyi(words))
