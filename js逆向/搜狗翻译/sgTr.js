var caiji
!function (t) {
    var r = {}

    function c(e) {
        if (r[e])
            return r[e].exports;
        var n = r[e] = {
            "i": e,
            "l": !1,
            "exports": {}
        };
        return t[e].call(n.exports, n, n.exports, c),
            n.l = !0,
            n.exports
    };
    caiji = c
}({
    143: function (t, n) {
        var e = {
            "utf8": {
                "stringToBytes": function (t) {
                    return e.bin.stringToBytes(unescape(encodeURIComponent(t)))
                },
                "bytesToString": function (t) {
                    return decodeURIComponent(escape(e.bin.bytesToString(t)))
                }
            },
            "bin": {
                "stringToBytes": function (t) {
                    for (var n = [], e = 0; e < t.length; e++)
                        n.push(255 & t.charCodeAt(e));
                    return n
                },
                "bytesToString": function (t) {
                    for (var n = [], e = 0; e < t.length; e++)
                        n.push(String.fromCharCode(t[e]));
                    return n.join("")
                }
            }
        };
        t.exports = e
    },
    339: function (t, n) {
        var e, r;
        e = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",
            r = {
                "rotl": function (t, n) {
                    return t << n | t >>> 32 - n
                },
                "rotr": function (t, n) {
                    return t << 32 - n | t >>> n
                },
                "endian": function (t) {
                    if (t.constructor == Number)
                        return 16711935 & r.rotl(t, 8) | 4278255360 & r.rotl(t, 24);
                    for (var n = 0; n < t.length; n++)
                        t[n] = r.endian(t[n]);
                    return t
                },
                "randomBytes": function (t) {
                    for (var n = []; t > 0; t--)
                        n.push(Math.floor(256 * Math.random()));
                    return n
                },
                "bytesToWords": function (t) {
                    for (var n = [], e = 0, r = 0; e < t.length; e++,
                        r += 8)
                        n[r >>> 5] |= t[e] << 24 - r % 32;
                    return n
                },
                "wordsToBytes": function (t) {
                    for (var n = [], e = 0; e < 32 * t.length; e += 8)
                        n.push(t[e >>> 5] >>> 24 - e % 32 & 255);
                    return n
                },
                "bytesToHex": function (t) {
                    for (var n = [], e = 0; e < t.length; e++)
                        n.push((t[e] >>> 4).toString(16)),
                            n.push((15 & t[e]).toString(16));
                    return n.join("")
                },
                "hexToBytes": function (t) {
                    for (var n = [], e = 0; e < t.length; e += 2)
                        n.push(parseInt(t.substr(e, 2), 16));
                    return n
                },
                "bytesToBase64": function (t) {
                    for (var n = [], r = 0; r < t.length; r += 3)
                        for (var o = t[r] << 16 | t[r + 1] << 8 | t[r + 2], i = 0; i < 4; i++)
                            8 * r + 6 * i <= 8 * t.length ? n.push(e.charAt(o >>> 6 * (3 - i) & 63)) : n.push("=");
                    return n.join("")
                },
                "base64ToBytes": function (t) {
                    t = t.replace(/[^A-Z0-9+\/]/gi, "");
                    for (var n = [], r = 0, o = 0; r < t.length; o = ++r % 4)
                        0 != o && n.push((e.indexOf(t.charAt(r - 1)) & Math.pow(2, -2 * o + 8) - 1) << 2 * o | e.indexOf(t.charAt(r)) >>> 6 - 2 * o);
                    return n
                }
            },
            t.exports = r
    },
    340: function (t, n) {
        function e(t) {
            return !!t.constructor && "function" == typeof t.constructor.isBuffer && t.constructor.isBuffer(t)
        }

        t.exports = function (t) {
            return null != t && (e(t) || function (t) {
                return "function" == typeof t.readFloatLE && "function" == typeof t.slice && e(t.slice(0, 0))
            }(t) || !!t._isBuffer)
        }
    },

});


function cond() {
    var e = caiji
    return {
        "crypt": e(339),
        "utf8": e(143).utf8,
        "isBuffer": e(340),
        "bin": e(143).bin
    }
}

function md5(t, n) {
    t.constructor == String ? t = n && "binary" === n.encoding ? cond().bin.stringToBytes(t) : cond().utf8.stringToBytes(t) : cond().isBuffer(t) ? t = Array.prototype.slice.call(t, 0) : Array.isArray(t) || (t = t.toString());
    for (var e = cond().crypt.bytesToWords(t), r = 8 * t.length, o = 1732584193, i = -271733879, a = -1732584194, u = 271733878, c = 0; c < e.length; c++)
        e[c] = 16711935 & (e[c] << 8 | e[c] >>> 24) | 4278255360 & (e[c] << 24 | e[c] >>> 8);
    e[r >>> 5] |= 128 << r % 32,
        e[14 + (r + 64 >>> 9 << 4)] = r;
    var f = _ff
        , s = _gg
        , l = _hh
        , p = _ii;
    for (c = 0; c < e.length; c += 16) {
        var h = o
            , v = i
            , d = a
            , g = u;
        o = f(o, i, a, u, e[c + 0], 7, -680876936),
            u = f(u, o, i, a, e[c + 1], 12, -389564586),
            a = f(a, u, o, i, e[c + 2], 17, 606105819),
            i = f(i, a, u, o, e[c + 3], 22, -1044525330),
            o = f(o, i, a, u, e[c + 4], 7, -176418897),
            u = f(u, o, i, a, e[c + 5], 12, 1200080426),
            a = f(a, u, o, i, e[c + 6], 17, -1473231341),
            i = f(i, a, u, o, e[c + 7], 22, -45705983),
            o = f(o, i, a, u, e[c + 8], 7, 1770035416),
            u = f(u, o, i, a, e[c + 9], 12, -1958414417),
            a = f(a, u, o, i, e[c + 10], 17, -42063),
            i = f(i, a, u, o, e[c + 11], 22, -1990404162),
            o = f(o, i, a, u, e[c + 12], 7, 1804603682),
            u = f(u, o, i, a, e[c + 13], 12, -40341101),
            a = f(a, u, o, i, e[c + 14], 17, -1502002290),
            o = s(o, i = f(i, a, u, o, e[c + 15], 22, 1236535329), a, u, e[c + 1], 5, -165796510),
            u = s(u, o, i, a, e[c + 6], 9, -1069501632),
            a = s(a, u, o, i, e[c + 11], 14, 643717713),
            i = s(i, a, u, o, e[c + 0], 20, -373897302),
            o = s(o, i, a, u, e[c + 5], 5, -701558691),
            u = s(u, o, i, a, e[c + 10], 9, 38016083),
            a = s(a, u, o, i, e[c + 15], 14, -660478335),
            i = s(i, a, u, o, e[c + 4], 20, -405537848),
            o = s(o, i, a, u, e[c + 9], 5, 568446438),
            u = s(u, o, i, a, e[c + 14], 9, -1019803690),
            a = s(a, u, o, i, e[c + 3], 14, -187363961),
            i = s(i, a, u, o, e[c + 8], 20, 1163531501),
            o = s(o, i, a, u, e[c + 13], 5, -1444681467),
            u = s(u, o, i, a, e[c + 2], 9, -51403784),
            a = s(a, u, o, i, e[c + 7], 14, 1735328473),
            o = l(o, i = s(i, a, u, o, e[c + 12], 20, -1926607734), a, u, e[c + 5], 4, -378558),
            u = l(u, o, i, a, e[c + 8], 11, -2022574463),
            a = l(a, u, o, i, e[c + 11], 16, 1839030562),
            i = l(i, a, u, o, e[c + 14], 23, -35309556),
            o = l(o, i, a, u, e[c + 1], 4, -1530992060),
            u = l(u, o, i, a, e[c + 4], 11, 1272893353),
            a = l(a, u, o, i, e[c + 7], 16, -155497632),
            i = l(i, a, u, o, e[c + 10], 23, -1094730640),
            o = l(o, i, a, u, e[c + 13], 4, 681279174),
            u = l(u, o, i, a, e[c + 0], 11, -358537222),
            a = l(a, u, o, i, e[c + 3], 16, -722521979),
            i = l(i, a, u, o, e[c + 6], 23, 76029189),
            o = l(o, i, a, u, e[c + 9], 4, -640364487),
            u = l(u, o, i, a, e[c + 12], 11, -421815835),
            a = l(a, u, o, i, e[c + 15], 16, 530742520),
            o = p(o, i = l(i, a, u, o, e[c + 2], 23, -995338651), a, u, e[c + 0], 6, -198630844),
            u = p(u, o, i, a, e[c + 7], 10, 1126891415),
            a = p(a, u, o, i, e[c + 14], 15, -1416354905),
            i = p(i, a, u, o, e[c + 5], 21, -57434055),
            o = p(o, i, a, u, e[c + 12], 6, 1700485571),
            u = p(u, o, i, a, e[c + 3], 10, -1894986606),
            a = p(a, u, o, i, e[c + 10], 15, -1051523),
            i = p(i, a, u, o, e[c + 1], 21, -2054922799),
            o = p(o, i, a, u, e[c + 8], 6, 1873313359),
            u = p(u, o, i, a, e[c + 15], 10, -30611744),
            a = p(a, u, o, i, e[c + 6], 15, -1560198380),
            i = p(i, a, u, o, e[c + 13], 21, 1309151649),
            o = p(o, i, a, u, e[c + 4], 6, -145523070),
            u = p(u, o, i, a, e[c + 11], 10, -1120210379),
            a = p(a, u, o, i, e[c + 2], 15, 718787259),
            i = p(i, a, u, o, e[c + 9], 21, -343485551),
            o = o + h >>> 0,
            i = i + v >>> 0,
            a = a + d >>> 0,
            u = u + g >>> 0
    }
    return cond().crypt.endian([o, i, a, u])
}

function _ff(t, n, e, r, o, i, a) {
    var u = t + (n & e | ~n & r) + (o >>> 0) + a;
    return (u << i | u >>> 32 - i) + n
}

function _gg(t, n, e, r, o, i, a) {
    var u = t + (n & r | e & ~r) + (o >>> 0) + a;
    return (u << i | u >>> 32 - i) + n
}

function _hh(t, n, e, r, o, i, a) {
    var u = t + (n ^ e ^ r) + (o >>> 0) + a;
    return (u << i | u >>> 32 - i) + n
}

function _ii(t, n, e, r, o, i, a) {
    var u = t + (e ^ (n | ~r)) + (o >>> 0) + a;
    return (u << i | u >>> 32 - i) + n
}

var blocksize = 16

function cal(t) {
    var n = undefined
    if (null == t)
        throw new Error("Illegal argument " + t);
    var e = cond().crypt.wordsToBytes(md5(t, n));
    return n && n.asBytes ? e : n && n.asString ? cond().bin.bytesToString(e) : cond().crypt.bytesToHex(e)
}


function fanyi(word) {
    var g = "auto", h = "en", O = 109984457
    word_ = "".concat(g).concat(h).concat(word).concat(O)
    console.log(word_)
    return cal(word_)
}

console.log(fanyi('你好'));

