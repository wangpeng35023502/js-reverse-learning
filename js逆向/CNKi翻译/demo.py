﻿# -*- encoding: utf-8 -*-

import requests
import json
import execjs


def words(key):
    with open('translateSDK.js', 'r', encoding='utf-8') as f:
        js = f.read()
        words = execjs.compile(js).call('encryptWords', key)
        return words


headers = {
    "Accept": "application/json, text/plain, */*",
    "Accept-Language": "zh,zh-CN;q=0.9",
    "Cache-Control": "no-cache",
    "Connection": "keep-alive",
    "Content-Type": "application/json;charset=UTF-8",
    "Origin": "https://dict.cnki.net",
    "Pragma": "no-cache",
    "Referer": "https://dict.cnki.net/index",
    "Sec-Fetch-Dest": "empty",
    "Sec-Fetch-Mode": "cors",
    "Sec-Fetch-Site": "same-origin",
    "Token": "ed0bd6e5-b15e-4331-a10a-d11a885e7e5b",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36",
    "sec-ch-ua": "\".Not/A)Brand\";v=\"99\", \"Google Chrome\";v=\"103\", \"Chromium\";v=\"103\"",
    "sec-ch-ua-mobile": "?0",
    "sec-ch-ua-platform": "\"Windows\""
}

url = "https://dict.cnki.net/fyzs-front-api/translate/literaltranslation"
data = {
    "words": words('你好'),
    "translateType": None
}
data = json.dumps(data)
response = requests.post(url, headers=headers, data=data)

mResult = response.json()['data']['mResult']
print("翻译结果:" + mResult)
