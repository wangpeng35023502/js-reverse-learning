﻿const CryptoJs = require('crypto-js');

var n = "4e87183cfd3a45fe";
function encryptWords(t) {
    var e = {
        mode: CryptoJs.mode.ECB,
        padding: CryptoJs.pad.Pkcs7
    }
        , i = CryptoJs.enc.Utf8.parse(n)
        , s = CryptoJs.AES.encrypt(t, i, e)
        , r = s.toString().replace(/\//g, "_");
    return r.replace(/\+/g, "-");
};
function decryptWords(t) {
            var e = t.replace(/\-/g, "+").replace(/_/g, "/")
              , i = {
                mode: CryptoJs.mode.ECB,
                padding: CryptoJs.pad.Pkcs7
            }
              , s = CryptoJs.enc.Utf8.parse(n)
              , r = CryptoJs.AES.decrypt(e, s, i)
              , o = CryptoJs.enc.Utf8.stringify(r);
            return o
        }
console.log(encryptWords('你好'));