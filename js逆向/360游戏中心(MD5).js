function t(e, t) {
var n = (65535 & e) + (65535 & t),
i = (e >> 16) + (t >> 16) + (n >> 16);
return i << 16 | 65535 & n
}
function n(e, t) {
return e << t | e >>> 32 - t
}
function i(e, i, o, r, a, c) {
return t(n(t(t(i, e), t(r, c)), a), o)
}
function o(e, t, n, o, r, a, c) {
return i(t & n | ~t & o, e, t, r, a, c)
}
function r(e, t, n, o, r, a, c) {
return i(t & o | n & ~o, e, t, r, a, c)
}
function a(e, t, n, o, r, a, c) {
return i(t ^ n ^ o, e, t, r, a, c)
}
function c(e, t, n, o, r, a, c) {
return i(n ^ (t | ~o), e, t, r, a, c)
}
function s(e, n) {
e[n >> 5] |= 128 << n % 32,
e[(n + 64 >>> 9 << 4) + 14] = n;
var i, s, u, l, p, d = 1732584193,
f = -271733879,
h = -1732584194,
m = 271733878;
for (i = 0; i < e.length; i += 16) s = d,
u = f,
l = h,
p = m,
d = o(d, f, h, m, e[i], 7, -680876936),
m = o(m, d, f, h, e[i + 1], 12, -389564586),
h = o(h, m, d, f, e[i + 2], 17, 606105819),
f = o(f, h, m, d, e[i + 3], 22, -1044525330),
d = o(d, f, h, m, e[i + 4], 7, -176418897),
m = o(m, d, f, h, e[i + 5], 12, 1200080426),
h = o(h, m, d, f, e[i + 6], 17, -1473231341),
f = o(f, h, m, d, e[i + 7], 22, -45705983),
d = o(d, f, h, m, e[i + 8], 7, 1770035416),
m = o(m, d, f, h, e[i + 9], 12, -1958414417),
h = o(h, m, d, f, e[i + 10], 17, -42063),
f = o(f, h, m, d, e[i + 11], 22, -1990404162),
d = o(d, f, h, m, e[i + 12], 7, 1804603682),
m = o(m, d, f, h, e[i + 13], 12, -40341101),
h = o(h, m, d, f, e[i + 14], 17, -1502002290),
f = o(f, h, m, d, e[i + 15], 22, 1236535329),
d = r(d, f, h, m, e[i + 1], 5, -165796510),
m = r(m, d, f, h, e[i + 6], 9, -1069501632),
h = r(h, m, d, f, e[i + 11], 14, 643717713),
f = r(f, h, m, d, e[i], 20, -373897302),
d = r(d, f, h, m, e[i + 5], 5, -701558691),
m = r(m, d, f, h, e[i + 10], 9, 38016083),
h = r(h, m, d, f, e[i + 15], 14, -660478335),
f = r(f, h, m, d, e[i + 4], 20, -405537848),
d = r(d, f, h, m, e[i + 9], 5, 568446438),
m = r(m, d, f, h, e[i + 14], 9, -1019803690),
h = r(h, m, d, f, e[i + 3], 14, -187363961),
f = r(f, h, m, d, e[i + 8], 20, 1163531501),
d = r(d, f, h, m, e[i + 13], 5, -1444681467),
m = r(m, d, f, h, e[i + 2], 9, -51403784),
h = r(h, m, d, f, e[i + 7], 14, 1735328473),
f = r(f, h, m, d, e[i + 12], 20, -1926607734),
d = a(d, f, h, m, e[i + 5], 4, -378558),
m = a(m, d, f, h, e[i + 8], 11, -2022574463),
h = a(h, m, d, f, e[i + 11], 16, 1839030562),
f = a(f, h, m, d, e[i + 14], 23, -35309556),
d = a(d, f, h, m, e[i + 1], 4, -1530992060),
m = a(m, d, f, h, e[i + 4], 11, 1272893353),
h = a(h, m, d, f, e[i + 7], 16, -155497632),
f = a(f, h, m, d, e[i + 10], 23, -1094730640),
d = a(d, f, h, m, e[i + 13], 4, 681279174),
m = a(m, d, f, h, e[i], 11, -358537222),
h = a(h, m, d, f, e[i + 3], 16, -722521979),
f = a(f, h, m, d, e[i + 6], 23, 76029189),
d = a(d, f, h, m, e[i + 9], 4, -640364487),
m = a(m, d, f, h, e[i + 12], 11, -421815835),
h = a(h, m, d, f, e[i + 15], 16, 530742520),
f = a(f, h, m, d, e[i + 2], 23, -995338651),
d = c(d, f, h, m, e[i], 6, -198630844),
m = c(m, d, f, h, e[i + 7], 10, 1126891415),
h = c(h, m, d, f, e[i + 14], 15, -1416354905),
f = c(f, h, m, d, e[i + 5], 21, -57434055),
d = c(d, f, h, m, e[i + 12], 6, 1700485571),
m = c(m, d, f, h, e[i + 3], 10, -1894986606),
h = c(h, m, d, f, e[i + 10], 15, -1051523),
f = c(f, h, m, d, e[i + 1], 21, -2054922799),
d = c(d, f, h, m, e[i + 8], 6, 1873313359),
m = c(m, d, f, h, e[i + 15], 10, -30611744),
h = c(h, m, d, f, e[i + 6], 15, -1560198380),
f = c(f, h, m, d, e[i + 13], 21, 1309151649),
d = c(d, f, h, m, e[i + 4], 6, -145523070),
m = c(m, d, f, h, e[i + 11], 10, -1120210379),
h = c(h, m, d, f, e[i + 2], 15, 718787259),
f = c(f, h, m, d, e[i + 9], 21, -343485551),
d = t(d, s),
f = t(f, u),
h = t(h, l),
m = t(m, p);
return [d, f, h, m]
}
function u(e) {
var t, n = "";
for (t = 0; t < 32 * e.length; t += 8) n += String.fromCharCode(e[t >> 5] >>> t % 32 & 255);
return n
}
function l(e) {
var t, n = [];
for (n[(e.length >> 2) - 1] = void 0, t = 0; t < n.length; t += 1) n[t] = 0;
for (t = 0; t < 8 * e.length; t += 8) n[t >> 5] |= (255 & e.charCodeAt(t / 8)) << t % 32;
return n
}
function p(e) {
return u(s(l(e), 8 * e.length))
}
function d(e, t) {
var n, i, o = l(e),
r = [],
a = [];
for (r[15] = a[15] = void 0, o.length > 16 && (o = s(o, 8 * e.length)), n = 0; n < 16; n += 1) r[n] = 909522486 ^ o[n],
a[n] = 1549556828 ^ o[n];
return i = s(r.concat(l(t)), 512 + 8 * t.length),
u(s(a.concat(i), 640))
}
function f(e) {
var t, n, i = "0123456789abcdef",
o = "";
for (n = 0; n < e.length; n += 1) t = e.charCodeAt(n),
o += i.charAt(t >>> 4 & 15) + i.charAt(15 & t);
return o
}
function h(e) {
return unescape(encodeURIComponent(e))
}
function m(e) {
return p(h(e))
}
function g(e) {
return f(m(e))
}
function v(e, t) {
return d(h(e), h(t))
}
function q(e, t) {
return f(v(e, t))
}
md5 = function(e, t) {
return t ? q(t, e) : g(e)
};

console.log(md5('111111'));