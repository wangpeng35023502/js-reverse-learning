﻿# -*- encoding: utf-8 -*-
import json
import re

import execjs
import requests

base_url = 'https://www.mafengwo.cn/'
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36'
}
session = requests.Session()


def get_first_cookies():
    res = session.get(url=base_url, headers=headers)
    pattern = re.compile('<script>document\.cookie=(.*?);location', re.S)
    js = re.findall(pattern, res.text)[0]
    jsl_clearance_s = execjs.eval(js).split(';')[0]
    return jsl_clearance_s


def get_second_cookies():
    first_cookies = get_first_cookies().split('=')
    cookies = {first_cookies[0]: first_cookies[1]}
    res = session.get(url=base_url, headers=headers, cookies=cookies)
    data = re.findall(';go\((.*?)\)</script>', res.text)[0]
    data = json.loads(data)
    with open('jsl.js', 'r', encoding='utf-8') as f:
        jsl_js = f.read()
        result = execjs.compile(jsl_js).call('getCookies', data)
        return result


def get_html():
    second_cookies = get_second_cookies()
    res = session.get(url=base_url, headers=headers, cookies=second_cookies)
    return res.text


if __name__ == '__main__':
    html = get_html()
    print(html)
